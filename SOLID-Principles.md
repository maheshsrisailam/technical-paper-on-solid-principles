#  SOLID

## Introduction

SOLID are the five principle which helps us to create good software architecture. In the SOLID word, each 
letter represents one principle like this,

S  ---  Single Responsibility Principle

O  ---  Open-Closed Principle

L  ---  Liskov Substitution Principle

I  ---  Interface Segregration Principle

D  ---  Dependency Inversion Principle 

Using these principles, we can build efficient, reusable and non-fragile software which is sustainable and maintainable for long period needs.

## Single Responsibility Principle

The Single Responsibility Principle states that all of our classes, functions, modules etc anything inside our 
code, should have only one responsiblity to perform.

Here SRP is not followed. This Memory storage tracker class has two responsibilites like, it has to track the storage and if exceeds it has to log the message as "Memory is exceedeed" like this.


    class MemoryStorageTracker {
        constructor(maximunStorage){
            this.maximunStorage = maximunStorage
            this.currentStorage = 0
        }                                                                                                        
        trackMeroryStorage(usedStorage) {
            this.currentStorage += usedStorage
            if (this.currentStorage > this.maximunStorage) {
                this.logExcessStorage()
            }else{
                console.log(this.currentStorage)
            }                                                                                                    
        }                                                                                                        
        logExcessStorage() {
            console.log('Unable to store because maximum storage is reached')
        }
    }

    const memoryStorageTracker = new MemoryStorageTracker(512)
    memoryStorageTracker.trackMeroryStorage(150)
    memoryStorageTracker.trackMeroryStorage(220)
    memoryStorageTracker.trackMeroryStorage(200)



Here we are removing the logExcessStorage method from the class and wrote it in the separate module and whereever we want, we can use that module in our code. Now it can follow the SRP. The MemoryStorageTracker is performing one task which is tracking the storage.


    const excessStorageMessageLogger = require('./excessStorageMessageLogger')                                       
    class MemoryStorageTracker {
        constructor(maximunStorage){
            this.maximunStorage = maximunStorage
            this.currentStorage = 0
        }                                                                                                        
        trackMeroryStorage(usedStorage) {
            this.currentStorage += usedStorage
            if (this.currentStorage > this.maximunStorage) {
                excessStorageMessageLogger()
            }else{
                console.log(this.currentStorage)
            }
        }
    }

    const memoryStorageTracker = new MemoryStorageTracker(512)
    memoryStorageTracker.trackMeroryStorage(150)
    memoryStorageTracker.trackMeroryStorage(220)
    memoryStorageTracker.trackMeroryStorage(200)

excessStorageMessageLogger.js   

    const excessStorageMessageLogger = () => console.log('Unable to store because maximum storage is reached')

    module.exports = excessStorageMessageLogger

## Open-Closed Principle

Open-Closed Principle states that classes, functions or modules should be open for extensions and closed for the
modification. Here modification means changing the existing code which is not a good practice and extension means adding new functionality or feature without modifying the existing code.


Here this function is violating the open closed principle. In this example we are modifying the existing code every time we are adding new role to this function. Instead we can divide this whole function into individual functions where we can't touch the existing code.

But this code follows the single responsibility principle.


    function checkEmployeeDesignationType(role) {
        if (role==="fullstackdeveloper"){
            console.log("I am full stack developer")
        } else if (role==="frontenddeveloper"){
            console.log("I am frontend developer")
        }else if (role==="backenddeveloper"){
            console.log("I am backend developer")
        }else if (role==="tester"){
            console.log("I am tester")
        }else if (role==="hrmanager"){
            console.log("I am HR manager")
        }
    }


Here we are adding the new role without touching or modifying the existing code by adding new functions.
This code follows the open-closed principle.


    function checkForFullStackDeveloper(role) {
        if (role === 'fullstackdeveloper') {
            console.log("I am the Full Stack Developer")
        }
    }
    function checkForFrontendDeveloper(role) {
        if (role === 'frontenddeveloper') {
            console.log("I am the Frontend Developer ")
        }
    }
    function checkForBackendDeveloper(role) {
        if (role === 'backenddeveloper') {
            console.log("I am the Backend Developer")
        }
    }
    function checkForTester(role) {
        if (role === 'tester') {
            console.log("I am the Tester")
        }
    }
    function checkForHR(role) {
        if (role === 'hr') {
            console.log("I am the HR")
        }
    }   

    checkForFullStackDeveloper('fullstackdeveloper')
    checkForBackendDeveloper('backenddeveloper')
    checkForFrontendDeveloper('frontenddeveloper')
    checkForHR('hr')
    checkForTester("tester")

## Liskov Substitution Principle


Liskov substitution principle states that the object of super class should be replacable with the object of its sub classes. Object of the sub-class should behave in same way as its super class object.
Here in this code, sub classes of the Authorizations class should behave exactly as the parent class or super class.


    class Authorizations {
        signup() {
            console.log('Success! Account is created.')
        }
        login() {
            console.log('Successfully Logged in to the account')
        }
        deleteUser() {
            console.log("User is deleted successfully.")
        }
    }

    class Admin extends Authorizations {}
    class GuestUser extends Authorizations {}

    let authorizations = new Authorizations()

    let admin = new Admin()
    admin.signup()
    admin.login()
    admin.deleteUser() // This code works fine for admin

    let user = new GuestUser() 
    user.signup()
    user.login()
    user.deleteUser() // Again this code works fine for guest user also.


In the real time scenario, there is no access to the guest user to delete the existing user right. Here it violates the Liskov Substitution Principle.

In the below code, it follows the Liskov Substitution Principle. Here we are removing the delete user method from the Authorizations class and provided in the Admin class.


    class Authorizations {
        signup() {
            console.log('Success! Account is created.')
        }
        login() {
            console.log('Successfully Logged in to the account')
        }
    }
    class Admin extends Authorizations {
        deleteUser() {
            console.log("User is deleted successfully.")
        }
    }
    class GuestUser extends Authorizations {}

    let authorizations = new Authorizations()

    let admin = new Admin()
    admin.signup()
    admin.login()
    admin.deleteUser()

    let user = new GuestUser() 
    user.signup()
    user.login()

## Interface Segregation Principle

Interface Segregation Principle states that, if we have a large interface which provides multiple services to us. Maintaining the single large interface is not a good practice instead we can divide the large interface into smaller and individual interfaces which can perform specific task. 

In order to utilize and implement the concept of abstraction, we can implement abstractions using the interfaces in the Object-oriented designs. The main purpose of using interfaces is to hide all the implementation details.


    class ATMMachine {
        cashWithdrawal() {
            console.log("Cash is withdrawn successfully.")
        }
        balanceEnquiry() {
            console.log('Available balance is: XXXXXXXX')
        }
        cashDeposit() {
            console.log("Cash is deposited successfully")
        }
        chequeDeposit() {
            console.log("Cheque is deposited successfully")
        }
    }

    let atmMachine = new ATMMachine()
    atmMachine.cashWithdrawal()
    atmMachine.balanceEnquiry()
    atmMachine.cashDeposit()
    atmMachine.chequeDeposit()


In the above code, it performs functionalities with the single interface called ATM machine. Here, if we can observe carefully the customers are waiting for the services,in this case, all the customers including customers who want to withdrawn cash, who want to check balance in their account, who want to cash deposit and who want to cheque deposit. Instead of providing multiple functionalities to the one interface, we can divide the interface into multiple small interfaces like one interface for cash withdrawal and balance enquiry and one for cash deposit and another one for cheque deposit.


    class CashWithdrawalAndBalanceEnquiryMachine {
        cashWithdrawal() {
            console.log("Cash is withdrawn successfully")
        }
        balanceEnquiry() {
            console.log('Available balance is: XXXX7845')
        }
    }

    class CashDepositMachine {
        cashDeposit() {
            console.log('Cash deposited successfully')
        }
    }
    class ChequeDepositMachine {
        chequeDeposit() {
            console.log('Cheque is deposited successfully')
        }
    }

    let cashWithdrawalAndBalanceEnquiryMachine = new CashWithdrawalAndBalanceEnquiryMachine()
    cashWithdrawalAndBalanceEnquiryMachine.cashWithdrawal()
    cashWithdrawalAndBalanceEnquiryMachine.balanceEnquiry()

    let cashDepositMachine = new CashDepositMachine()
    cashDepositMachine.cashDeposit()

    let chequeDepositMachine = new ChequeDepositMachine()
    chequeDepositMachine.chequeDeposit()

## Dependency Inversion Principle
Dependency Inversion Principle states that high-level modules should not depend on the low-level modules.
Both should depend on the abstraction. Abstraction should not depend on the implementaion but implementation 
depends on the abstraction. 

We can implement abstractions using interfaces.

Code which doesn't follows the dependency inversion principle

    class Store {
        constructor(user) {
            this.user = user
            this.netbanking = new NetBanking(user)
            //this.upi = new UPI(user)
        }

        purchaseBike(quantity) {
            this.netbanking.makePayment(200000 * quantity)
            //this.upi.makePayment(200000* quantity)
        }

        purchaseHelmet(quantity) {
            this.netbanking.makePayment(1500 * quantity)
            //this.upi.makePayment(1500 * quantity)
        }
    }

    class UPI {
        constructor(user) {
            this.user = user
        }

        makePayment(amountInRupees) {
            console.log(`${this.user} made payment of $${amountInRupees} with UPI`)
        }
    }

    class NetBanking {
        constructor(user) {
            this.user = user
        }
        makePayment(amountInRupees) {
            console.log(`${this.user} made payment of $${amountInRupees} with Netbanking`)
        }
    }

    const store = new Store('John')
    store.purchaseBike(2)
    store.purchaseHelmet(2)
 
Code which follows the dependency inversion principle

    class Store {
        constructor(paymentProcessor) {
            this.paymentProcessor = paymentProcessor
        }

        purchaseBike(quantity) {
            this.paymentProcessor.pay(200000 * quantity)
        }

        purchaseHelmet(quantity) {
            this.paymentProcessor.pay( 1500 * quantity)
        }
    }


    class UPIPaymentProcessor {
        constructor(user) {
            this.upi = new UPI(user)
        }

        pay(amountInRupees) {
            this.upi.makePayment(amountInRupees)
        }
    }

    class NetBankingPaymentProcessor {
        constructor(user) {
            this.user = user
            this.netbanking = new NetBanking(user)
        }

        pay(amountInRupees) {
            this.netbanking.makePayment(amountInRupees)
        }
    }

    class UPI {
        constructor(user) {
            this.user = user
        }

        makePayment(amountInCents) {
            console.log(`${this.user} made payment of RS: ${amountInCents} with UPI`)
        }
    }

    class NetBanking {
        constructor(user) {
            this.user = user
        }
        makePayment(amountInDollars) {
            console.log(`${this.user} made payment of RS: ${amountInDollars} with NetBanking`)
        }
    }

    const store = new Store(new UPIPaymentProcessor('John'))
    //const store = new Store(new NetBankingPaymentProcessor('John'))
    store.purchaseBike(2)
    store.purchaseHelmet(2)


## Conclusion

By following these SOLID Principles, we can build efficient, reusable and non-fragile software which is sustainable and maintainable for long period needs.
